import { useState } from "react";

export default function Form({ onAddItems }) {
  const [description, setDescription] = useState("");
  const [amount, setAmount] = useState("1");

  function handleSubmit(e) {
    e.preventDefault();

    if (!description) return;
    if (!amount) return;

    const newItem = {
      id: amount + description + Date.now(),
      description,
      amount,
      packed: false,
    };

    onAddItems(newItem);

    setDescription("");
    setAmount("1");
  }

  return (
    <>
      <h2>What will you pack to survive?</h2>

      <form className="add-form" onSubmit={handleSubmit}>
        <div>
          <input
            type="text"
            placeholder="Item..."
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          />
          <input
            type="text"
            placeholder="Amount..."
            value={amount}
            onChange={(e) => {
              setAmount(e.target.value);
            }}
          />
        </div>
        <button>Add</button>
      </form>
    </>
  );
}
