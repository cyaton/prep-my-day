import logo from "../assets/firelogs-512x512.png";

export default function Logo() {
  return (
    <>
      <img src={logo} className="logo" alt="logo" />
      <h1>Prep My Day</h1>
    </>
  );
}
