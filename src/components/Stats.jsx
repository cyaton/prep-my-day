// This component renders some stats displayed in the footer
export default function Stats({ items }) {
  if (!items.length)
    return (
      <footer className="stats">Start adding items to your packing list</footer>
    );
  // Derived state variables
  const numItems = items.length;
  const numItemsPacked = items.filter((item) => item.packed).length;
  const percentPacked =
    items.length === 0 ? 0 : Math.round((numItemsPacked / numItems) * 100);
  console.log(percentPacked);

  return (
    <footer className="stats">
      {percentPacked === 100
        ? "You are ready to go!"
        : `You have ${numItems} items on your list, and you already packed ${percentPacked}%`}
    </footer>
  );
}
