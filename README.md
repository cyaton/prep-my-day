# Prep My Day

## Description

Prep My Day is a frontend showcase project for react

- It lets you add and remove items to a packing list
- Frontend built with [React](https://react.dev/)/vite

## Installation

Simply run it in the browser, you can [visit Prep My Day here](https://prep-my-day.netlify.app/)

## Usage

- Just add the name of the item to pack and mark it crossed out when packed

### Privacy

All entered data is displayed locally on your browser without persistence on any app-side backend

## Support

Feel free to contact me on [GitLab](https://gitlab.com/cyaton).

## Feedback

Any feedback, suggestions or collaboration attempts are welcome! Contact me on [GitLab](https://gitlab.com/cyaton).

## Roadmap

Project finished

## Authors and acknowledgment

Contributors: Philipp Montazem

### Acknowledgments

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

#### Jonas Schmedtmann

Thanks [to Jonas Schmedtmann's great React Courses](http://jonas.io/) which heavily inspired this project!

## LICENSE

Copyright (C) 2023 Philipp Montazem

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Project finished. Readme last updated on October 22nd, 2023